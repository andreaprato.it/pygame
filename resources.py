import pygame
import os

def load_image (immagine, scale=0, rotate=0):
    base_path = os.path.dirname(__file__)+"/immagini"
    img_path=os.path.join(base_path, immagine)
    img=pygame.image.load(img_path)
    if scale>0:
        img = pygame.transform.scale(img, (scale, scale))
    if rotate>0:
        img = pygame.transform.rotate(img, rotate)
    return img

def load_music(nome):
    base_path = os.path.dirname(__file__)+"/suoni"
    img_path=os.path.join(base_path, nome)
    return pygame.mixer.music.load(img_path)


def load_sound(nome):
    base_path = os.path.dirname(__file__)+"/suoni"
    img_path=os.path.join(base_path, nome)
    return pygame.mixer.Sound(img_path)