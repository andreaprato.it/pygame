import pygame

# Inizializza il modulo Pygame
pygame.init()

# Imposta la risoluzione dello schermo
screen_width, screen_height = pygame.display.Info().current_w, pygame.display.Info().current_h
screen = pygame.display.set_mode((screen_width, screen_height), pygame.FULLSCREEN)

# Imposta il colore dello sfondo come nero
black = (0, 0, 0)
screen.fill(black)

# Aggiorna lo schermo
pygame.display.update()

# Esegui il gioco in un ciclo infinito
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

# Esci dal modulo Pygame
 if (event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE)):
