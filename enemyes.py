import random
import pygame
import os
import math
import random

from resources import load_image
from resources import load_sound
from resources import load_music

# pygame.draw.circle(screen, (255,255,255), (800, 600), 5)

e1=None
e2=None
enemyesList = []
bulletsList = []
p=None
pygame.init()

def sin(ang: int):
    return math.sin(math.pi*ang/180)

laserSound=load_sound("laser.wav")

music=load_music("music_enemy.mp3")
pygame.mixer.music.play(-1)

def enemyes(screen,w,h,proiettileX,proiettileY):
    global enemyesList
    global p
   
    if len(enemyesList) == 0: 
        for i in range(10):
            
            enemyesList.append(Enemy(screen,w,h,0,0))

    for e in enemyesList:
        e.generatePosition(generatePosition)
        e.draw()
    
    for b in bulletsList:
        b.draw()
    


class Bullet:
    
    def  __init__ (self,screen,w,h,x,y):
        self.screen=screen
        self.bullet = load_image("lasers/03.png",64,180)
        self.width=w
        self.height=h
        
        self.bulletX=x
        self.bulletY=y
        self.fired=False
    
    def draw(self):
        global laserSound
        # pygame.draw.circle(self.screen, (255,117,20), ( self.bulletX, self.bulletY), 5)
        if not self.fired:
            laserSound.set_volume(0.2)
            laserSound.play()
            self.fired=True
        self.screen.blit(self.bullet,( self.bulletX, self.bulletY))
        self.bulletX=self.bulletX - 8     


def generatePosition(x,y,ang,vel,slope,maxY):
    ang = ang + 1 
        
    if ang == 360:
        ang = 0
        vel =random.randint(-2,2)
        slope =random.randint(-maxY, maxY )
    
    nX = x + vel
    nY = int(slope*sin(ang))

    return  (nX , nY, ang, vel, slope) 
    

class Enemy:
        
         
    def  __init__ (self,screen,w,h,x,y):
        self.screen=screen
        self.dimAlien = 64
        self.alien = load_image("alien_spaceship_invasion_1.png",self.dimAlien,270.0)
        self.oX=w - self.dimAlien
        self.oY=(h>>1)-(self.dimAlien>>1)
        
        self.width=w
        self.height=h
        # self.maxY=(self.height >> 1) - (self.dimAlien >> 1)
        self.ang = random.randint(0,360)
        self.vel =random.randint(-2,2)
        self.slope =random.randint(-self.oY, self.oY )
        
        self.alienX = x
        self.alienY = y
        
        self.tick = random.randint(100,300)
        
    def generatePosition(self, generator):
        newState=generator(self.alienX,self.alienY,self.ang,self.vel,self.slope,self.oY)
        
        self.alienX=newState[0]
        self.alienY=newState[1]
        
        self.ang=newState[2]
        self.vel=newState[3]
        self.slope=newState[4]
    
    def fire(self):
        global bulletsList
        proiettile=Bullet(self.screen,self.width,self.height,self.oX - self.alienX, self.oY - self.alienY)
        bulletsList.append(proiettile)
        # proiettile.fire()
    
        
    def _draw(self,x,y):
        nx=self.oX - x 
        ny=self.oY - y
        if nx < 0 or nx > self.width - self.dimAlien:
            self.reset() 
            return
        if ny < 0 or ny > self.height - self.dimAlien:
            self.reset()
            return
        self.screen.blit(self.alien,( nx ,ny ))
    
    
    def draw(self):
        self._draw(self.alienX,self.alienY)
        self.tick-=1
        if (self.tick==0):
            self.tick = random.randint(100,300)
            self.fire()    

    def reset (self) :
        # self.ang = - self.ang 
        self.vel = - self.vel
        
        
        
