import pygame
import os
import random

def carica (immagine):
    base_path = os.path.dirname(__file__)+"/immagini"
    img_path=os.path.join(base_path, immagine)
    return pygame.image.load(img_path)

# Funzioni
def setup():
    global WIDTH, HEIGHT
    global CENTER_X,CENTER_Y
    global SCREEN
    global STAR_COLOR
    global STAR_DIM
    global STAR_Z_ORIGIN
    global FPS
    global stars
    global zeta 
    global PERSPECTIVE_CORRECTION
    
    STAR_COLOR = (255,255,255) # WHITE
    NUM_STARS = 1000
    STAR_DIM=5
    STAR_Z_ORIGIN=255
    FPS=25
    PERSPECTIVE_CORRECTION=4

    SCREEN=pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    infoObject = pygame.display.Info()
    WIDTH=infoObject.current_w
    HEIGHT=infoObject.current_h
    CENTER_X=int(WIDTH/2)
    CENTER_Y=int(HEIGHT/2)
    stars= []
    for count in range(0,NUM_STARS):
        star=create_star()
        stars.append(star)

def create_star(distance=-1):
    if distance==-1: distance=random.randint(1,STAR_Z_ORIGIN)
    return (random.randint(-CENTER_X,CENTER_X),random.randint(-CENTER_Y,CENTER_Y),distance)

def paint_star_rect(xx,yy):
    pygame.draw.rect(SCREEN,STAR_COLOR,(xx,yy,STAR_DIM,STAR_DIM))

def paint_star_pixel(xx,yy):
    SCREEN.set_at((xx,yy), STAR_COLOR)

def paint():
    for (x,y,z) in stars:
        xx=CENTER_X+int(x*4/z)
        yy=CENTER_Y+int(y*4/z)
        paint_star_rect(xx,yy)
        #paint_star_pixel(xx,yy)

def update():
    pygame.display.flip()
    pygame.time.Clock().tick(FPS)
    i=0
    for (x,y,z) in stars:
        z=z-1
        if (z==0): stars[i]= star=create_star(STAR_Z_ORIGIN)
        else: stars[i]= (x,y,z)
        i=i+1

# Programma
pygame.init()
setup()

while True:
    SCREEN.fill((0,0,0))
    for event in pygame.event.get():
        if (event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE)):
            pygame.quit()
    paint()
    update()