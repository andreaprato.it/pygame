import random
import pygame
import os

from enemyes import enemyes
from resources import load_image



# Inizializza il modulo Pygame
pygame.init()

global screen_width
global screen_height
global screen

# Imposta la risoluzione dello schermo
screen_width, screen_height = pygame.display.Info().current_w, pygame.display.Info().current_h
screen = pygame.display.set_mode((screen_width, screen_height), pygame.FULLSCREEN)

dimAstronave = 128
astronave = load_image("ship1.png")
astronave = pygame.transform.scale(astronave, (dimAstronave, dimAstronave))

bg = load_image("bg2.jpg")
bg=pygame.transform.scale(bg, (screen_width, screen_height))

# Imposta il colore dello sfondo come nero
black = (0, 0, 0)
screen.fill(black)

# Imposta il colore delle stelle come bianco
white = (255, 255, 255)
# Imposta il colore dei proiettili
red = (200 , 64 , 0)

# Definire la quantità di stelle
star_count = 200

# Crea una lista per memorizzare le coordinate delle stelle
stars = []
for i in range(star_count):
    x = random.randint(0, screen_width)
    y = random.randint(0, screen_height)
    stars.append([x, y])

# Imposta la velocità di spostamento delle stelle
star_speed = 4.5


pygame.key.set_repeat(10,10)
running = True
astronaveY = 300
astronaveX = 300
proiettileX=astronaveX+128
proiettileY=astronaveY+64
sparato=False

# Esegui il gioco in un ciclo infinito
while running:
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            running = False
        if event.type == pygame.KEYDOWN and event.key == pygame.K_UP :
            if  astronaveY-8 > 0:
                astronaveY=astronaveY-8
           
        if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN :
            if  astronaveY+8 < screen_height-dimAstronave:
                astronaveY=astronaveY+8
                
        if event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:        
            if  astronaveX-8 > 0:
                astronaveX=astronaveX-8       
        
        if event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:        
            if  astronaveX+8 <screen_width-dimAstronave:
                astronaveX=astronaveX+8
        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE and not sparato:
            sparato=True
            proiettileX=astronaveX+128
            proiettileY=astronaveY+64
    
    if sparato:
        if proiettileX+8 < screen_width:
            proiettileX=proiettileX+10      
        else:
            proiettileX=astronaveX+128
            sparato=False
        
    # Aggiorna la posizione delle stelle
    for i in range(star_count):
        stars[i][0] -= star_speed
        if stars[i][0] < 0:
            stars[i][0] = screen_width
            stars[i][1] = random.randint(0, screen_height)
    
    # Disegna le stelle sullo schermo
    screen.fill(black)
    screen.blit(bg,(0,0))
    
    for i in range(star_count):
        pygame.draw.circle(screen, white, (int(stars[i][0]), int(stars[i][1])), 2)
    
    # Disegno del proiettile
    if sparato:
        pygame.draw.circle(screen, red, (proiettileX, proiettileY), 5)
        
    screen.blit(astronave,(astronaveX, astronaveY))
    
    enemyes(screen,screen_width, screen_height,proiettileX,proiettileY)
    
    font = pygame.font.SysFont(None, 48)
    img = font.render('hello', True, red)
    screen.blit(img, (20, 20))
    
    # Aggiorna lo schermo
    pygame.display.update()

# Esci dal modulo Pygame
pygame.quit()

