from cmath import cos
import pygame

import os
import random
import numpy as np
import math as m

def carica (immagine):
    base_path = os.path.dirname(__file__)+"/immagini"
    img_path=os.path.join(base_path, immagine)
    return pygame.image.load(img_path)

def carica_musica(nome):
    base_path = os.path.dirname(__file__)+"/suoni"
    img_path=os.path.join(base_path, nome)
    return pygame.mixer.music.load(img_path)

# Funzioni
def setup():
    pygame.init()
    global WIDTH, HEIGHT
    global CENTER_X,CENTER_Y
    global SCREEN
    global STAR_COLOR
    global STAR_DIM
    global STAR_Z_ORIGIN
    global FPS
    global stars
    global starsRot
    global zeta 
    global PERSPECTIVE_CORRECTION
    global ANG
    global COS
    global SIN
    global VEL
    global RZ
    global RY
    global RX
    global bg

    STAR_COLOR = (255,255,255) # WHITE
    NUM_STARS = 500
    STAR_DIM=5
    STAR_Z_ORIGIN=255
    FPS=60
    PERSPECTIVE_CORRECTION=6
    
    ANG=0
    VEL=2

    SCREEN=pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    infoObject = pygame.display.Info()
    WIDTH=infoObject.current_w
    HEIGHT=infoObject.current_h
    CENTER_X=int(WIDTH/2)
    CENTER_Y=int(HEIGHT/2)
    
    stars= []
    starsRot= []
    for count in range(0,NUM_STARS):
        star=create_star()
        stars.append(star)
        starsRot.append(star)
    
    # # LookUp Tables
    RZ=[]
    RX=[]
    RY=[]
    COS=[]
    SIN=[]
    
    for a in range(360):
        COS.append(m.cos(a* m.pi/180))
        SIN.append(m.sin(a* m.pi/180))

        # Matrici di rotazione
        RX.append(Rx(a))
        RY.append(Ry(a))
        RZ.append(Rz(a))
    
    bg = carica("bg2.jpg")
    bg=pygame.transform.scale(bg, (WIDTH, HEIGHT))

    music=carica_musica("music.mp3")
    pygame.mixer.music.play(-1)
    pygame.time.delay(10000)
    

def create_star(distance=-1):
    if distance==-1: distance=random.randint(1,STAR_Z_ORIGIN)
    return (random.randint(-CENTER_Y,CENTER_Y),random.randint(-CENTER_Y,CENTER_Y),distance)

def calc_color(z):
    colFloat=1-z/STAR_Z_ORIGIN
    scaled=1 - m.sqrt(1 - colFloat**2 )
    color=int(255*colFloat)
    return (color,color,color)

def calc_dim(z):
    return int(4 * (1 - z/STAR_Z_ORIGIN))

def Rz(angolo):
  return np.array([
            [ COS[angolo], -SIN[angolo], 0 ],
            [ SIN[angolo], COS[angolo] , 0 ],
            [ 0           , 0          , 1 ] ])

def Rx(angolo):
  return np.array([
            [ 1, 0           , 0         ],
            [ 0, COS[angolo],-SIN[angolo]],
            [ 0, SIN[angolo],COS[angolo] ] ])

def Ry(angolo):
  return np.array([
            [ COS[angolo], 0, SIN[angolo]],
            [ 0           , 1, 0         ],
            [-SIN[angolo], 0, COS[angolo]] ])

def rotateZ(v,theta):
    vv=np.array([v[0],v[1],v[2]])
    r=vv.dot(RZ[theta])
    return (r[0],r[1],r[2])

def rotateY(v,theta):
    vv=np.array([v[0],v[1],v[2]])
    r=vv.dot(RY[theta])
    return (r[0],r[1],r[2])

def rotateX(v,theta):
    vv=np.array([v[0],v[1],v[2]])
    r=vv.dot(RX[theta])
    return (r[0],r[1],r[2])

def rotate(v,angX,angY,angZ):
    vv=np.array([v[0],v[1],v[2]])
    rr=RX[angX].dot(RY[angY]).dot(RZ[angZ])
    r=vv.dot(rr)
    return (r[0],r[1],r[2])

def regen_star(index):
    point=starsRot[index]
    
    if int(point[2])<=0:
        stars[index]=create_star(STAR_Z_ORIGIN)
        starsRot[index]=stars[index]

def paint_star(index):
    point=starsRot[index]
    
    x=int(point[0])
    y=int(point[1])
    z=int(point[2])
    
    xx=CENTER_X+(x<<PERSPECTIVE_CORRECTION)/z
    yy=CENTER_Y+(y<<PERSPECTIVE_CORRECTION)/z
    dim=calc_dim(z)
    color=calc_color(z)
    pygame.draw.rect(SCREEN,color,(xx,yy,dim,dim))

def move():
    i=0
    global ANG

    ANG=ANG+VEL
    if ANG==360: ANG=0

    for (x,y,z) in stars:
        z=z-VEL
        stars[i]=(x,y,z)
        starsRot[i]=rotateZ(stars[i],0)    
        i+=1


def paint_stars():
    for i in range(len(starsRot)):
        regen_star(i)
        paint_star(i)

def paint_background():
    SCREEN.blit(bg, (0, 0))

def update():
    pygame.display.flip()
#    pygame.time.Clock().tick(FPS)

# def fade(in_out):
#     if in_out==1:


def draw():
    move()
    paint_background()
    paint_stars()

#STARS 3D
setup()

alphaSurface = pygame.Surface((WIDTH,HEIGHT)) # The custom-surface of the size of the screen.
alphaSurface.fill((0,0,0)) # Fill it with whole white before the main-loop.
alph =255 # The increment-variable.
alphaSurface.set_alpha(alph)
while True:

    for event in pygame.event.get():
        if (event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE)):
            while alph<255:
                pygame.mixer.music.fadeout(5000)
                alph+=2
                alphaSurface.set_alpha(alph)
                draw()
                SCREEN.blit(alphaSurface, (0, 0))
                update()
            pygame.quit()

    if alph>0: alph -= 1 
    alphaSurface.set_alpha(alph)
    draw()
    SCREEN.blit(alphaSurface, (0, 0))
    update()
