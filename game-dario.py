import pygame
import os

def carica (immagine):
    base_path = os.path.dirname(__file__)+"/immagini"
    img_path=os.path.join(base_path, immagine)
    return pygame.image.load(img_path)

# Funzioni
    global uccellox, uccelloy, uccello_vely
    global basex
    uccellox = 60
    uccelloy = 150
    uccello_vely = 0
    basex= 0

def disegna():
    schermo.blit(sfondo,(0,0))
    schermo.blit(uccello,(uccellox, uccelloy))
    schermo.blit(base, (basex,400))

def aggiorna():
    pygame.display.update()
    pygame.time.Clock().tick(FPS)

def hai_perso():
    schermo.blit(gameover,(50,180))
    aggiorna()
    ricominciamo=False
    while not ricominciamo:
        for event in pygame.event.get():
            if (event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE):
                inizializza()
                ricominciamo = True
            if event.type == pygame.QUIT:
                pygame.quit()

pygame.init()

sfondo=carica("sfondo.png")
uccello=carica("uccello.png")
base=carica("base.png")
gameover= carica("gameover.png")
tubo_giu= carica("tubo.png")
tubo_su = pygame.transform.flip(tubo_giu,False,True)

# Inizializzato lo schermo
schermo = pygame.display.set_mode((288,512))
FPS = 50
vel_avanz = 3

# Programma
inizializza()

while True:
    basex -= vel_avanz
    if basex< -45: basex= 0

    uccello_vely+=1
    uccelloy+=uccello_vely
    for event in pygame.event.get():
        if (event.type == pygame.KEYDOWN and event.key == pygame.K_UP):
            uccello_vely= -10
        if event.type == pygame.QUIT:
            pygame.quit()
    if uccelloy >380 :
        hai_perso()
    disegna()
    aggiorna()